// Mengistu Shuma ID 108458839
// CSCI 2312 001
// Assigment Final Project
// Battleship program
// November 28, 2017

#include "player.h"
#include <iomanip>
#include <iostream>
#include <stdio.h>
#include <vector>
#include <cstring>
#include "exceptionHandling.h"
using namespace std;

// namespace used for the program
namespace BattelShipGame {
    
    // deafult constructor
    Player::Player()
    {
    }
    
    // void funtion that fill player grid with '-'
    void Player::filltheGrid()
    {
        // user for loop to fill the grid
        for( int i = 0; i < row;  i++ )
        {
            for (int j = 0; j < 10; j++)
            {
                // set each positon of the grid with '-'
                setPlayerShip[i][j] = '-';
                playerGame[i][j] = '-';
            }
        }
    }
    
    // functin that obtain the user input and display if the input is hit or miss
    void Player::PlayerInput(char my[row][col])
    {
        // variable that counts how may times that the user hit a ship
         static int countCarrier = 0, countBattleship = 0, countCruiser = 0, countSubmarine = 0, countDetroyer = 0;
        // variable that holds -99 when the user guess the position of the computer ship
        int checkCarrier = 0, checkBattleship = 0, checkCruiser = 0, checkSubmarine = 0, checkDetroyer = 0, checkMiss = 0;
     // lable
    start:
        // obtian user input
        string input;
        // user prompt
        cout << "It is your turn. Fire a torpedo: ";
        cin >> input; // obtain user input
         int x = 0, y = 0 ;
        
        // set user input 2 and 3 letter to char2 and char3
        char char2 = input[1];
        char char3 = input[2];
      
        
        
       // validate user input if it is A to J and the second letter is 1 to 10
        while (input[0] < 'A' || input[0] > 'J' || input[1] < '0' || input[1] > '9')
        {
            cout << "Wrong input!! please enter letter A to J and number 1 to 10 " << endl;
            cout << "Example (A1): ";
            cin.ignore();
            cin >> input;
        }
       
        // change the first letter to x integer and the second letter
        // to y
        if (input[0] == 'A' )
        {x = 1; y = input[1] - 48; }
        else if(input[0] ==  'B' )
        {x = 2; y = input[1] - 48; }
        else if(input[0] ==  'C')
        { x = 3; y = input[1] - 48;}
        else if(input[0] ==  'D' )
        { x = 4; y = input[1] - 48;}
        else if (input[0] ==  'E' )
        { x = 5; y = input[1] - 48; }
        else if (input[0] ==  'F' )
        { x = 6; y = input[1] - 48; }
        else if(input[0] ==  'G' )
        { x = 7; y = input[1] - 48; }
        else if (input[0] ==  'H' )
        { x = 8; y = input[1] - 48; }
        else if (input[0] ==  'I' )
        {  x = 9; y = input[1] - 48;}
        else if (input[0] ==  'J' )
        { x = 10; y = input[1] - 48; }
        else
        {
            cout << "Error Wrong input " << x << " " << y << endl;
        }
        // if the second and third letter is 1 and 0 set y to 10
        if(char2 == '1' && char3 == '0')
            y = 10;
        
        cout << endl;
        cout << "\t\tPlayer grid" << endl;
        // string array that holds the row of the grid
        string  rowL[] = {"A", "B", "C", "D", "E" , "F", "G", "H", "I", "J"};
        cout << "     ";
        // loop the grid
        for (int i = 0; i < row; i++)
            // display row of the grid
            cout  <<  rowL[i] << setw(3);
        cout << endl;
        // use for loop to loop every position of the grid
        for( int i = 0; i < row;  i++ )
        {
            // dispaly the col of the grid
            cout << i+1 << setw(3);
            for (int j = 0; j < col; j++)
            {
             
                // check if the computer grid has carrier ship at the position of the user guess
                if(my[y-1][x-1] == 'C')
                {
                    // mark player grid with hit
                    playerGame[y-1][x-1] = 'O';
                    checkCarrier = -99;
                }
                
                // check if the computer grid has battle ship at the position of the user guess
                else if(my[y-1][x-1] == 'B')
                {
                     // mark player grid with hit
                    playerGame[y-1][x-1] = 'O';
                    checkBattleship = -99;
                   
                }
                 // check if the computer grid has cruiser ship at the position of the user guess
                else if(my[y-1][x-1] == 'R')
                {
                     // mark player grid with hit
                    playerGame[y-1][x-1] = 'O';
                    checkCruiser = -99;
                }
                // check if the computer grid has submarine ship at the position of the user guess
                else if(my[y-1][x-1] == 'S')
                {
                     // mark player grid with hit
                    playerGame[y-1][x-1] = 'O';
                    checkSubmarine = -99;
                }
                 // check if the computer grid has detroyer ship at the position of the user guess
                else if(my[y-1][x-1] == 'D')
                {
                     // mark player grid with hit
                    playerGame[y-1][x-1] = 'O';
                    checkDetroyer = -99;
                }
                
                else if(my[y-1][x-1] == 'O')
                {   cout << "You have already guessed this position please guess again" << endl;
                    goto start;
                }
             
                else
                {
                    // else set as miss
                    playerGame[y-1][x-1] = 'X';
                    checkMiss = -99;
                }
                
                // display the status
                cout << playerGame[i][j] << setw(3);
            }
            cout << endl;
      
        }
     
       // if the checkCarrier variable is -99, display that the user hit the ship
        if(checkCarrier == -99)
        {
             countCarrier++;
             cout << "\nYou hit Carrier ship " << endl;
            
        }
        
        // if the checkBattle variable is -99, display that the user hit the ship
        if(checkBattleship == -99)
        {
             countBattleship++;
              cout << "\nYou hit Battleship " << endl;
            
        }
        
         // if the checkCruiser variable is -99, display that the user hit the ship
        if(checkCruiser == -99)
        {
            countCruiser++;
             cout << "\nYou hit my cruiser ship " << endl;
            
        }
        
         // if the checkSubmarine variable is -99, display that the user hit the ship
        if(checkSubmarine == -99)
        {
             countSubmarine++;
            cout << "\nYou hit Submarine ship " << endl;
            
        }
        
         // if the checkDetrouer variable is -99, display that the user hit the ship
        if(checkDetroyer == -99)
        {
            countDetroyer++;
            cout << "\nYou hit detroyer ship " << endl;
        }
        
        // if checkMiss variable is -99, display that the user missed the ship
        if(checkMiss == -99)
            cout << "\nYou missed" << endl;
        // if the coutnCarrier equal to the  length of the  carrier ship, display the player sunk the  carrier ship
        if(countCarrier == 5)
            cout << "\nYou sunck computer carrier ship " << endl;
        // if the coutnBattleship equal to the  length of the  battle ship, display the player sunk the Battle ship
        if(countBattleship == 4)
            cout << "\nYou sunck  computer Battleship " << endl;
       // if the coutnCruiser equal to the  length of the  Cruiser ship, display the player sunk the  cruiser ship
        if(countCruiser == 3)
            cout << "\nYou sunck  computer cruiser ship " << endl;
        // if the coutnSubmarine equal to the  length of the  submarine ship, display the player sunk the  submarine ship
        if(countSubmarine == 3)
            cout << "\nYou sunck  computer submarine ship " << endl;
        // if the coutnDetroyer equal to the  length of the  detroyer ship, display the player sunk the  detroyer ship
        if(countDetroyer == 2)
            cout << "\nYou sunck  computer detroyer ship " << endl;
        
    }
    
    // void function that reads data from file
    void Player::readFromFile(ifstream& infile)
    {
        // variable that holds the type, location and orientation of the ship
        string  shipF, locationF, orientationF, message;
        vector <string> ship, location, orientation;
        
        // use exception handling
        try{
            // if there is not file throw message
            if(!infile)
                throw message;
            // while it is goos
            while (infile.good())
            {
                // read type of ship from file
                getline (infile, shipF, ',');
                // if the file is not in right format throw exception
                if(!infile.is_open() || shipF == " ")
                    throw ExceptionHandling();
                ship.push_back(shipF);
                
                // read location of ship from file
                getline (infile, locationF, ',');
                // if the file is not in right format throw exception
                if(!infile.is_open() || locationF == " ")
                    throw ExceptionHandling();
                location.push_back(locationF);
          
                // read orientation of ship from file
                infile >>  orientationF;
                // if the file is not in right format throw exception
                if(!infile.is_open() || orientationF == " ")
                    throw ExceptionHandling();
                orientation.push_back(orientationF);
          
            }
            
        } // end try
        
        // catch exception
        catch (string message)
        {
            cout << "Unable to open the file " << endl;
        }
    
        // catch exception
        catch ( ExceptionHandling e)
        {
            cout <<  e.getMessage() << endl;
        }
        

        int i = 1;
        int index = 0;
        // this for loop get rid TypeOfShip, Location, HorizOrVet that we read from file
        // and saves the rest of the file into array of ship, loaction and orientaiton
        for(i; i < ship.size(); i++)
        {
            shipp[index] = ship[i];
            locationn[index] = location[i];
            orientationn[index] = orientation[i];
            index++;
        }
    }
    
    // void fuction that sets the player ship on the grid
    void Player::setCarrierShip()
    {
        // set the length of the ship
        int carrier = 5;
        int carrierX = 0;
        int carrierY = 0;
        // variable that throw exception
        string orientation, overLap;
        int outOfRange = 0;
        // declare variable
        string x, y, carr, loc, orn;
        // read carrier data from array and set into locaal variable
        carr = shipp[0];
        loc = locationn[0];
        x = loc[0];
        y = loc[1];
        // cnange the the location of the string to integer number x and y
        carrierY = atoi(y.c_str()) - 1;
        
        // set the orientation of carrier ship
        orientation = orientationn[0];
        
        // use exception try and catch
        try{
        
            // set the location of row and col from 1 to 10
        if (x ==  "A" ) {carrierX  = 0;}
        else if(x ==  "B" ) {carrierX  = 1;}
        else if(x ==  "C" ) {carrierX  = 2;}
        else if(x ==  "D" ) {carrierX  = 3;}
        else if(x ==  "E" ) {carrierX  = 4;}
        else if(x ==  "F" ) {carrierX  = 5;}
        else if(x ==  "G" ) {carrierX  = 6;}
        else if(x ==  "H" ) {carrierX  = 7;}
        else if(x ==  "I" ) {carrierX  = 8;}
        else if(x ==  "J" ) {carrierX  = 9;}
          // esle
        else
        {
            cout << "Error Wrong input " << endl;
        }
        
            // if orentaiton is horizontal
        if (orientation == "H")
        {
            // if the location of input file plus length of carrier ship is out of range, throw exception
            if(carrierX + carrier >= row)
            {
                throw outOfRange;
            }
            
            else
            {
                // else check if there is a ship in the laocation. if there is a ship, throw excepation
                for(int i = carrierX; i < carrierX + carrier; i++)
                {
                    if( setPlayerShip[carrierX][i] == 'C' || setPlayerShip[carrierX][i] == 'B' ||setPlayerShip[carrierX][i] == 'S' ||setPlayerShip[carrierX][i] == 'S' ||setPlayerShip[carrierX][i] == 'D')
                        throw overLap;
                    else
                    // if there is no ship set the carrier ship
                    setPlayerShip[carrierY][i] = 'C';
                }
            }
            
        } // end if
            // if orientation is vertical
        if (orientation == "V")
        {
             // if the location of input file plus length of Battle ship is out of range, throw exception
            if (carrierY + carrier >= col)
                throw outOfRange;
            else
            {
             
                // else check if there is a ship in the laocation. if there is a ship throw excepation
                for(int i = carrierY; i < carrierY + carrier; i++)
                {
                    if( setPlayerShip[i][carrierX] == 'C' || setPlayerShip[i][carrierX] == 'B' || setPlayerShip[i][carrierX] == 'S' ||setPlayerShip[i][carrierX] == 'S' || setPlayerShip[i][carrierX] == 'D')
                        throw overLap;
                    else
                          // if there is no ship set the carrier ship
                        setPlayerShip[i][carrierX] = 'C';
                }
            }// end if
        }
        
    }
    
    // catch excepiton if it is overlap
        catch(string overlap)
        {
        cout << "The ship overLap please set your ships again" << endl ;
        }
        // catch exception if it is out of range
        catch(int OutOfRange)
        {
            cout << "The ship is out of range" << endl;
        }
    }
    // void function that sets the battle ship
    void Player::setBattleShip()
    {
        // call carriership function
        setCarrierShip();
        // declare variable that holds the length, x and y location of the sip
        int battle = 4;
        int battleX = 0;
        int battleY = 0;
        int outOfRange = 0;
        string orientation, overLap;
        
        // set the battle ship data from array
        string x, y, carr, loc, orn;
        carr = shipp[1];
        loc = locationn[1];
        x = loc[0];
        y = loc[1];
        //change the location to integer number
        battleY = atoi(y.c_str()) - 1;
        orientation = orientationn[1];
        
        try {
        // set the location of row and col from 1 to 10
        if (x ==  "A" ) {battleX  = 0;}
        else if(x ==  "B" ) {battleX  = 1;}
        else if(x ==  "C" ) {battleX  = 2;}
        else if(x ==  "D" ) {battleX  = 3;}
        else if(x ==  "E" ) {battleX  = 4;}
        else if(x ==  "F" ) {battleX  = 5;}
        else if(x ==  "G" ) {battleX  = 6;}
        else if(x ==  "H" ) {battleX  = 7;}
        else if(x ==  "I" ) {battleX  = 8;}
        else if(x ==  "J" ) {battleX  = 9;}
        // else display worng input
        else
        {
            cout << "Error Wrong input " << endl;
        }
        
        // if orientation is horizontal
        if (orientation == "H")
        {
             // if the location of input file plus length of battle ship is out of range, throw exception
            if(battleX + battle >= row)
            {
                throw outOfRange;
            }
            else
            {
                // else check if there is a ship in the laocation. If there is a ship, throw excepation
                for(int i = battleX; i < battleX + battle; i++)
                {
                    if( setPlayerShip[battleX][i] == 'C' || setPlayerShip[battleX][i] == 'B' || setPlayerShip[battleX][i]== 'S' ||setPlayerShip[battleX][i] == 'S' || setPlayerShip[battleX][i] == 'D')
                        throw overLap;
                    else
                      // if there is no ship set the battle ship
                    setPlayerShip[battleY][i] = 'B';
                }
            }
            
        } // end if
        
         // if orientation is vertical
        if (orientation == "V")
        {
             // if the location of input file plus length of battle ship is out of range, throw exception
            if (battleY + battle >= col)
                throw outOfRange;
            else
            {
                // else check if there is a ship in the laocation. If there is a ship, throw excepation
                for(int i = battleY; i < battleY + battle; i++)
                {
                    if( setPlayerShip[i][battleX] == 'C' || setPlayerShip[i][battleX] == 'B' || setPlayerShip[i][battleX] == 'S' ||setPlayerShip[i][battleX] == 'S' || setPlayerShip[i][battleX] == 'D')
                        throw overLap;
                        
                    else
                          // if there is no ship set the battle ship
                    setPlayerShip[i][battleX] = 'B';
                }
                
            }
        }
        }
        // catch exception if it is overlap
        catch ( string overLap)
        {
            cout << "The ship overLap please set your ships again" << endl ;
        }
        // catch exception if it is out of range
        catch(int OutOfRange)
        {
            cout << "The ship is out of range" << endl;
        }
        
    }
    
    
    // void function that set cruiser ship
    void Player:: setCruiserShip()
    {
     // cal setBattleship function
        setBattleShip();
        // declare cruiser ship length
        int cruiser = 3;
        int cruiserX = 0;
        int cruiserY = 0;
        int outOfRange = 0;
        string orientation, overLap;
        
         // set the cruiser ship data from array
        string x, y, carr, loc, orn;
        carr = shipp[2];
        loc = locationn[2];
        x = loc[0];
        y = loc[1];
        // change the orientation, x and y to integer number
        cruiserY = atoi(y.c_str()) - 1;
        orientation = orientationn[2];
        
        // try
        try
        {
        //according to the data input change the value of the x to number
        if (x ==  "A" ) {cruiserX  = 0;}
        else if(x ==  "B" ) {cruiserX  = 1;}
        else if(x ==  "C" ) {cruiserX  = 2;}
        else if(x ==  "D" ) {cruiserX  = 3;}
        else if(x ==  "E" ) {cruiserX  = 4;}
        else if(x ==  "F" ) {cruiserX  = 5;}
        else if(x ==  "G" ) {cruiserX  = 6;}
        else if(x ==  "H" ) {cruiserX  = 7;}
        else if(x ==  "I" ) {cruiserX  = 8;}
        else if(x ==  "J" ) {cruiserX  = 9;}
        else
        {
            cout << "Error rong input " << endl;
        }
        
        
        
        // if orientaition is horizontal
        if (orientation == "H")
        {
            // if the location of input file plus length of cruiser ship is out of range, throw exception
            if(cruiserX + cruiser >= row)
            {
                throw outOfRange;
            }
            else
            {
                   // else check if there is a ship in the laocation. If there is a ship, throw excepation
                for(int i = cruiserX; i < cruiserX + cruiser; i++)
                {
                    if( setPlayerShip[cruiserX][i] == 'C' || setPlayerShip[cruiserX][i] == 'B' || setPlayerShip[cruiserX][i] == 'S' ||setPlayerShip[cruiserX][i] == 'S' || setPlayerShip[cruiserX][i] == 'D')
                        throw overLap;
                    else
                         // if there is no ship set the cruiser ship
                    setPlayerShip[cruiserY][i] = 'R';
                }
            }
            
        }// end if
            
            
          // if orientaition is vertical
        if (orientation == "V")
        {
             // if the location of input file plus length of cruiser ship is out of range, throw exception
            if (cruiserY + cruiser >= col)
                throw outOfRange;
            else
            {
                // else check if there is a ship in the laocation. If there is a ship, throw excepation
                for(int i = cruiserY; i < cruiserY + cruiser; i++)
                {
                    if( setPlayerShip[i][cruiserX] == 'C' || setPlayerShip[i][cruiserX]  == 'B' || setPlayerShip[i][cruiserX]  == 'S' ||setPlayerShip[i][cruiserX]  == 'S' || setPlayerShip[i][cruiserX] == 'D')
                        
                        throw overLap;
                    else
                     // if there is no ship set the cruiser ship
                    setPlayerShip[i][cruiserX] = 'R';
                }
            }
        }
        }
        // catch exception if it is overlap
        catch ( string overLap)
        {
            cout << "The ship overLap please set your ships again" << endl ;
        }
        // catch exception if it is out of range
        catch(int OutOfRange)
        {
            cout << "The ship is out of range" << endl;
        }
        
    }
    
    // void function that set submarine ship
    void Player::setSubmarineShip()
    {
        // call cruiser function
        setCruiserShip();
        // declare the length of the submarine, x and y ship
        int submarine = 3;
        int submarineX = 0;
        int submarineY = 0;
        int outOfRange = 0;
        string orientation, overLap;
        
        // set submarine data from array
        string x, y, carr, loc, orn;
        carr = shipp[3];
        loc = locationn[3];
        x = loc[0];
        y = loc[1];
        
         // change the orientation, x and y to integer number
        submarineY = atoi(y.c_str()) - 1;
        orientation = orientationn[3];
        
        //try
        try {
         //according to the data input change the value of the x to number
        if (x ==  "A" ) {submarineX  = 0;}
        else if(x ==  "B" ) {submarineX  = 1;}
        else if(x ==  "C" ) {submarineX  = 2;}
        else if(x ==  "D" ) {submarineX  = 3;}
        else if(x ==  "E" ) {submarineX  = 4;}
        else if(x ==  "F" ) {submarineX  = 5;}
        else if(x ==  "G" ) {submarineX  = 6;}
        else if(x ==  "H" ) {submarineX  = 7;}
        else if(x ==  "I" ) {submarineX  = 8;}
        else if(x ==  "J" ) {submarineX  = 9;}
        else
        {
            cout << "Error rong input " << endl;
        }
        
        
        
          // if orientaition is horizontal
        if (orientation == "H")
        {
            
            // if the location of input file plus length of submarine ship is out of range, throw exception
            if(submarineX + submarine >= row)
            {
                throw outOfRange;
            }
            else
            {
                
                // else check if there is a ship in the laocation. If there is a ship, throw excepation
                for(int i = submarineX; i < submarineX + submarine; i++)
                {
                    if( setPlayerShip[submarineX][i] == 'C' || setPlayerShip[submarineX][i]  == 'B' || setPlayerShip[submarineX][i] == 'S' ||setPlayerShip[submarineX][i]  == 'S' || setPlayerShip[submarineX][i] == 'D')
                        throw overLap;
                else
                  // if there is no ship set the submarine ship
                    setPlayerShip[submarineY][i] = 'S';
                }
            }
            
        }// end if
            
        // if orinentation is vertrical
        if (orientation == "V")
        {
            // if the location of input file plus length of submarin ship is out of range, throw exception
            if (submarineY + submarine >= col)
                throw outOfRange;
            else
            {
                // else check if there is a ship in the laocation. If there is a ship, throw excepation
                for(int i = submarineY; i < submarineY + submarine; i++)
                {
                    if( setPlayerShip[i][submarineX] == 'C' || setPlayerShip[i][submarineX]  == 'B' || setPlayerShip[i][submarineX] == 'S' ||setPlayerShip[i][submarineX] == 'S' || setPlayerShip[i][submarineX] == 'D')
                        cout << "Error!! the ship overlaps " << endl;
                else
                      // if there is no ship set the submarine ship
                    setPlayerShip[i][submarineX] = 'S';
                }
            }
        }
        }
        // catch exception if it is overlap
        catch ( string overLap)
        {
            cout << "The ship overLap please set your ships again" << endl ;
        }
        // catch exception if it is out of range
        catch(int OutOfRange)
        {
            cout << "The ship is out of range" << endl;
        }
        
    }
    
    
    // void fucnction that set detroyer ship
    void Player::setDetroyerShip()
    {
        // call submarine ship function
        setSubmarineShip();
          // declare the length of the detroyer, x and y ship
        int detroyer = 2;
        int detroyerX = 0;
        int detroyerY = 0;
        int outOfrange = 0;
        string orientation, overLap;
        
        
        // set the detroyer ship data from array
        string x, y, carr, loc, orn;
        carr = shipp[4];
        loc = locationn[4];
        x = loc[0];
        y = loc[1];
        // set detroyer data from array
        detroyerY = atoi(y.c_str()) - 1;
        orientation = orientationn[4];
        
        
        //try
        try {
       //according to the data input change the value of the x to number
        if (x ==  "A" ) {detroyerX  = 0;}
        else if(x ==  "B" ) {detroyerX  = 1;}
        else if(x ==  "C" ) {detroyerX  = 2;}
        else if(x ==  "D" ) {detroyerX  = 3;}
        else if(x ==  "E" ) {detroyerX  = 4;}
        else if(x ==  "F" ) {detroyerX  = 5;}
        else if(x ==  "G" ) {detroyerX  = 6;}
        else if(x ==  "H" ) {detroyerX  = 7;}
        else if(x ==  "I" ) {detroyerX  = 8;}
        else if(x ==  "J" ) {detroyerX  = 9;}
        else
        {
            cout << "Error rong input " << endl;
        }
        
        
        
        // if orientation is horizontal
        if (orientation == "H")
        {
             // if the location of input file plus length of detroyer ship is out of range, throw exception
            if(detroyerX + detroyer >= row)
            {
                throw outOfrange;
            }
            else
            {
                // else check if there is a ship in the laocation. If there is a ship, throw excepation
                for(int i = detroyerX; i < detroyerX + detroyer; i++)
                {
                    if( setPlayerShip[detroyerX][i] == 'C' || setPlayerShip[detroyerX][i]  == 'B' || setPlayerShip[detroyerX][i] == 'S' ||setPlayerShip[detroyerX][i] == 'S' || setPlayerShip[detroyerX][i] == 'D')
                        cout << "Error!! the ship overlap " << endl;
                    else
                    // if there is no ship set the detrouey ship
                    setPlayerShip[detroyerY][i] = 'D';
                }
            }
            
        }// end if
        
        // if orientation is vetrtical
        if (orientation == "V")
        {
             // if the location of input file plus length of detroyer ship is out of range, throw exception
            if (detroyerY + detroyer >= col)
                throw outOfrange;
            else
            {
                // else check if there is a ship in the laocation. If there is a ship, throw excepation
                for(int i = detroyerY; i < detroyerY + detroyer; i++)
                {
                    if( setPlayerShip[i][detroyerX] == 'C' || setPlayerShip[i][detroyerX]  == 'B' || setPlayerShip[i][detroyerX] == 'S' ||setPlayerShip[i][detroyerX] == 'S' || setPlayerShip[i][detroyerX] == 'D')
                        cout << "Error!! the ship overlaps " << endl;
                    else
                        // if there is no ship set the detrouey ship
                    setPlayerShip[i][detroyerX] = 'D';
                }
            }
        }
        } // end try
        // catch exception if it is overlap
        catch ( string overLap)
        {
            cout << "The ship overLap please set your ships again" << endl ;
        }
        // catch exception if it is out of range
        catch(int OutOfRange)
        {
            cout << "The ship is out of range" << endl;
        }
      
    }
    
    // void function that print the player guess
    void Player::print()
    {
            // display all torped shot of user including if they hit or miss
            cout << "\n\t\tPlayer guess " << endl;
            string  rowL[] = {"A", "B", "C", "D", "E" , "F", "G", "H", "I", "J"};
            cout << "     ";
        
            // use for loop to step over the grid location
            for (int i = 0; i < row; i++)
                cout  <<  rowL[i] << setw(3);
            cout << endl;
            for( int i = 0; i < row; i++ )
            {
                cout << i+1 << setw(3);
                for (int j = 0; j < col; j++)
                {
                    // display the player grid containt
                    cout << playerGame[i][j] << setw(3);
                }
                cout << endl;
                
            }
            
        }
    // boolean function that check if the user win the game
    bool Player::winerPlayer()
    {
        // declare local variable
        int count = 0;
        // declare falge to false
        bool flag = false;
        
        // for loop counts how many mits on the grid
        for( int i = 0; i < 10; i++ )
        {
            for (int j = 0; j < 10; j++)
            {
                if(playerGame[i][j]== 'O')
                    count++;
            }
        }
        // if the hit is equal to 17 change the falge to true
        // because all length of the ships are 5+4+3+3+2 = 17
        if(count == 17)
            flag = true;
        // return falg
        return flag;
    }
    
}
