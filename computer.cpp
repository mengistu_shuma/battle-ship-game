
// Mengistu Shuma ID 108458839
// CSCI 2312 001
// Assigment Final Project
// Battleship program
// November 28, 2017

#include "computer.h"
#include <iostream>
#include <string>
#include <iomanip>
#include <ctime>
#include <stdlib.h>
#include "player.h"

// implementation of computer.h class
// namespace used for the program
namespace BattelShipGame {
    
// deafult constructor
    Computer::Computer()
    {
     // internationly empty
    }
    
// distractor
    Computer::~Computer()
    {
         // internationly empty
    }
// void fucntion that fills the grid by '-'
    void Computer::fillComputer()
    
    {
      
        for( int i = 0; i < 10; i++ )
        {
            for (int j = 0; j < 10; j++)
            {
                  // use for loop and fill the grid with '-'
                generateComputerships[i][j] = '-';
                computerGame[i][j] = '-';
            }
            cout << endl;
        }
    }
    
// void fucntion that generate and compare torpedo shot
    void Computer::generateTorpedo(char play[row][col])
    
    {
       // Declare static int that counts how many times that the ships hits
        static int countCarrier = 0, countBattleship = 0, countCruiser = 0, countSubmarine = 0, countDetroyer = 0;
        // declare variables that holds number -99 if torped hit the ship
        int checkCarrier = 0, checkBattleship = 0, checkCruiser = 0, checkSubmarine = 0, checkDetroyer = 0, checkMiss = 0;
     // label tos start form here
    startAllOver:
        // generate random torpedo shot x and y
        int y = rand() % col;
        int x = rand() % row;
        char input = '0';
        
        // according to random generated number set the value to y into letter from A to J
        if (y ==  0)
            input = 'A';
        else if (y ==  1)
            input = 'B';
        else  if (y ==  2)
            input = 'C';
        else if (y ==  3)
            input = 'D';
        else  if (y ==  4)
            input = 'E';
        else if (y ==  5)
            input = 'F';
        else if (y ==  6)
            input = 'G';
        else if (y ==  7)
            input = 'H';
        else if (y ==  8)
            input = 'I';
        else if (y ==  9)
            input = 'J';
        else
        {
            cout << "Error  " << endl;
        }
        
// display the grid of the computer to the user
        cout << endl;
        cout << "\t\tComputer grid" << endl;
        string  rowL[] = {"A", "B", "C", "D", "E" , "F", "G", "H", "I", "J"};
        cout << "     ";
        for (int i = 0; i < row; i++)
            cout  <<  rowL[i] << setw(3);
        cout << endl;
        for( int i = 0; i < row; i++ )
        {
            cout << i+1 << setw(3);
            for (int j = 0; j < col; j++)
            {
                // check if the player carrier ship is at generated torpedo shot
                if(play[x][y] == 'C')
                {
                    // if the carrier ship is at above position
                    // mark the computer grid as hit
                    computerGame[x][y] = 'O';
                    // set checkCarriesr -99 to display hit
                    checkCarrier = -99;
                }
    
                  // check if the player battleship is at generated torped shot
                else if(play[x][y] == 'B')
                {
                    // if the Battleship is at above position
                    // mark the computer grid as hit
                    computerGame[x][y] = 'O';
                    // set checkBattleship -99 to display hit
                    checkBattleship = -99;
                }
                
                     // check if the player Cruiser ship is at generated torped shot
                else if(play[x][y] == 'R')
                {
                    // if the cruiser ship is at above position
                    // mark the computer grid as hit
                    computerGame[x][y] = 'O';
                    // set checkCruiser -99 to display hit
                    checkCruiser = -99;
                }
                
                     // check if the player submarine ship is at generated torped shot
                else if(play[x][y] == 'S' )
                {
                    // if the submarine ship is at above position
                    // mark the computer grid as hit
                    computerGame[x][y] = 'O';
                      // set chekSubmarine -99 to display hit
                    checkSubmarine = -99;
                   
                    
                }
                 // check if the player detroyer ship is at generated torped shot
                else if(play[x][y] ==  'D')
                {
                    // if the detroyer ship is at above position
                    // mark the computer grid as hit
                    computerGame[x][y] = 'O';
                    checkDetroyer = -99;
                    
                }
                // if the torpedo was already fired that positon gerate another
                else if(play[x][y] == 'O')
                {
                    // go to lable startAllOver
                    goto startAllOver;
                }
             
                // if there is no ship at fired torpedo loction
                else
                {
                    // mark the computer grid with miss which is 'x'
                    computerGame[x][y] = 'X';
                    checkMiss = -99;
                }
                // display the status of the the grid
                 cout << computerGame[i][j] << setw(3);
                
                
            }
            cout << endl;
       
        }// end for loop
      
        // if chekCarries is -99 display as hit  and count how many times that the torpedo hits the
        // carrier ship
        if(checkCarrier == -99)
        {
            countCarrier++;
            cout << "\nComputer hits your carrier ship " << endl;
        }
        // if chekBattleShip is -99 display as hit  and count how many times that the torpedo hits the
        // Battleship
        if(checkBattleship == -99)
        {
            countBattleship++;
            cout << "\nComputer hits Battleship " << endl;
        }
        
        // if chekCruiser is -99 display as hit  and count how many times that the torpedo hits the
        // Cruiser ship
        if(checkCruiser == -99)
        {
            countCruiser++;
            cout << "\nComputer hits your cruiser ship " << endl;
        }
        
        // if chekSubmarine is -99 display as hit  and count how many times that the torpedo hits the
        // csubmarine ship
        if(checkSubmarine == -99)
        {
            countSubmarine++;
            cout << "\nComputer hits your submarine ship " << endl;
        }
        
        // if chekDetroyer is -99 display as hit  and count how many times that the torpedo hits the
        // Detroyer ship
        if(checkDetroyer == -99)
        {
            countDetroyer++;
            cout << "\ncomputer hits detroyer ship " << endl;
        }
        // if chcekMidd is -99 display as miss
        if(checkMiss == -99)
            cout << "\nComputer missed" << endl;
        
        // if countCarries equal to the length of carrier ship display as sunk
        if(countCarrier == 5)
            cout << "\nComputer sunck your Carrier ship" << endl;
        
         // if countCarries equal to the length of battle ship display as sunk
        if(countBattleship == 4)
            cout << "\nComputer sunck your Battleship " << endl;
        
         // if countCarries equal to the length of cruiser ship display as sunk
        if(countCruiser == 3)
            cout << "\nComputer sunck your cruiser ship " << endl;
        
         // if countCarries equal to the lenght of submarine ship display as sunk
        if(countSubmarine == 3)
            cout << "\nComputer sunck your submarine ship " << endl;
        
        // if countCarries equal to the length of detroyer ship display as sunk
        if(countDetroyer == 3)
            cout << "\nComputer sunck your detroyer ship " << endl;
        // displya ramdom generated torped shot
        cout << "\nRandom computer torpedo shot " << input << x+1 << endl;
    }
    
    // void function that generate random carrier ship
    void Computer::generateCarrierShip()
    {
        // helps to generate random different number
        srand(int (time(NULL)));
        // declare variable that holds the length of the carrier ship
        int carrier = 5;
        
        int vertical = 1; // set vertical to 1
        int horizontal = 0; // set horizontal to 0
        int orientation, carrierX, carrierY; // declare variables that holds x and y
        
    // lable
    startHere:
        // use do while loop to generate the random number
        do
        {
            
            orientation = rand() % 2; // generate vertical or horizontal
            carrierX = rand() % row; // generate random x location
            carrierY = rand() % col; // generate random y location
        } while (generateComputerships[carrierX][carrierY] == 'C'|| generateComputerships[carrierX][carrierY] == 'B' || generateComputerships[carrierX][carrierY] == 'R' || generateComputerships[carrierX][carrierY] == 'S' || generateComputerships[carrierX][carrierY] == 'D'); // check if the location has a ship
        
        // if the orintaion if horizontal
        if (orientation == horizontal)
        {
            // while the generated ramdom number plus the length of the ship is out of the grid
            while (carrierY + carrier >= col) { carrierY = rand() % row;} // generate random x location
            
            for(int i = carrierY; i < carrierY + carrier; i++)
            {
                // check if the new location and the length of the ship is occupied with ship
                if(generateComputerships[carrierX][i] == 'C' || generateComputerships[carrierX][i] == 'B' || generateComputerships[carrierX][i] == 'R' || generateComputerships[carrierX][i] == 'S' || generateComputerships[carrierX][i] == 'D' )
                    // if there is a ship at the position start again
                    goto startHere;
            }
            // if there is no ship at this position set the carrier ship
            for (int i = carrierY; i < carrierY + carrier; i++)
            {
                generateComputerships[carrierX][i] = 'C';
            }
            
        } // end if
        // if the orientation is vertical
        if (orientation == vertical)
        {
          // while the generated ramdom number plus the length of the ship is out of the grid
            while (carrierX + carrier >= row) { carrierX = rand() % col;} // generate random y location
         
            for(int i = carrierX; i < carrierX + carrier; i++)
            {
                 // check if the new location and the length of the ship is occupied with ship
                if(generateComputerships[i][carrierY] == 'C' || generateComputerships[i][carrierY] == 'B' || generateComputerships[i][carrierY] == 'R' || generateComputerships[i][carrierY] == 'S' || generateComputerships[i][carrierY] == 'D' )
                         // if there is a ship at the position start again
                    goto startHere;
            }
            // if there is no ship at this position set the carrier ship
            for (int i = carrierX; i < carrierX + carrier; i++)
            {
                generateComputerships[i][carrierY] = 'C';
            }
            
        }
    }
    
    // void function that generate random battleship
    void Computer::generateBattleShip()
    {
        // call function that generate carrier ship
        generateCarrierShip();
        // generate different number
        srand(int (time(NULL)));
        // set the length of the battleship
        int battleShip = 4;
        int vertical = 1; // set vertical = 1
        int horizontal = 0; // set horizontal = 1
        int orientation, BattleShipX, BattleShipY; // variables that holds x and y
        
        
    startHere:
        do
        {
            orientation = rand() % 2; // generate vertical or horizontal
            BattleShipX = rand() % 10; // generate random x location
            BattleShipY = rand() % 10;  // generate random y location
        } while (generateComputerships[BattleShipX][BattleShipY] == 'C' || generateComputerships[BattleShipX][BattleShipY]== 'B' || generateComputerships[BattleShipX][BattleShipY] == 'R' || generateComputerships[BattleShipX][BattleShipY] == 'S' || generateComputerships[BattleShipX][BattleShipY]== 'D'); // check if the location has a ship
          // if the orientation is vertical
        if (orientation == horizontal)
        {
            // while the generated ramdom number plus the length of the ship is out of the grid
            while (BattleShipY + battleShip >= col) { BattleShipY = rand() % col;} // generate new y location
            for(int i = BattleShipY; i < BattleShipY + battleShip; i++)
            {
                // check if the new location and the length of the ship is occupied with ship
                if(generateComputerships[BattleShipX][i] == 'C'  || generateComputerships[BattleShipX][i]== 'B' || generateComputerships[BattleShipX][i] == 'R' || generateComputerships[BattleShipX][i] == 'S' || generateComputerships[BattleShipX][i]== 'D')
                    // if there is a ship at the position start again
                    goto startHere;
            }
             // if there is no ship at this position set the battleship
            for (int i = BattleShipY; i < BattleShipY + battleShip; i++)
            {
                generateComputerships[BattleShipX][i] = 'B';
            }
            
        }// end if
        
        // if the orientation is vertical
        if (orientation == vertical)
        {
             // while the generated ramdom number plus the length of the ship is out of the grid
            while (BattleShipX + battleShip >= row) { BattleShipX = rand() % row;} // generate new x location
            for(int i = BattleShipX; i < BattleShipX + battleShip; i++)
            {
                // check if the new location and the length of the ship is occupied with ship
                if(generateComputerships[i][BattleShipY] == 'C' || generateComputerships[i][BattleShipY] == 'B' || generateComputerships[i][BattleShipY] == 'R' || generateComputerships[i][BattleShipY] == 'S' || generateComputerships[i][BattleShipY] == 'D')
                     // if there is a ship at the position start again
                    goto startHere;
            }
                  // if there is no ship at this position set the battleship
            for (int i = BattleShipX; i < BattleShipX + battleShip; i++)
            {
                generateComputerships[i][BattleShipY] = 'B';
            }
            
        } // end if
        
        
    }
    
     // void function that generate random cruiser ship
    void Computer::generateCruiserShip()
    {
         // call function that generate battle ship
        generateBattleShip();
         // generate different number
        srand(int (time(NULL)));
        // set the length of cruiser ship
        int cruiser = 3;
        int vertical = 1; // set vertical = 1
        int horizontal = 0; // ser horizontal = 0
        int orientation, cruiserX, cruiserY; // variable that holds x and y location
 
        
    startHere:
        do
        {
            
            orientation = rand() % 2; // generate vertical or horizontal
            cruiserX = rand() % row;  // generate random x location
            cruiserY = rand() % col; // generate random y location
        } while (generateComputerships[cruiserX][cruiserY] == 'C' || generateComputerships[cruiserX][cruiserY] == 'B' || generateComputerships[cruiserX][cruiserY] == 'R' || generateComputerships[cruiserX][cruiserY] == 'S' || generateComputerships[cruiserX][cruiserY] == 'D');  // check if the location has a ship
            // if the orientation is horizontal
        if (orientation == horizontal)
        {
            // while the generated ramdom number plus the length of the ship is out of the grid
            while (cruiserY + cruiser >= col) { cruiserY = rand() % col;} // generate new y location
            for(int i = cruiserY; i < cruiserY + cruiser; i++)
            {
                // check if the new location and the length of the ship is occupied with ship
                if(generateComputerships[cruiserX][i] == 'C' || generateComputerships[cruiserX][i] == 'B' || generateComputerships[cruiserX][i] == 'R' || generateComputerships[cruiserX][i] == 'S' || generateComputerships[cruiserX][i] == 'D')
                      // if there is a ship at the position start again
                    goto startHere;
            }
                  // if there is no ship at this position set the cruiser ship
            for (int i = cruiserY; i < cruiserY + cruiser; i++)
            {
                generateComputerships[cruiserX][i] = 'R';
            }
            
        } // endl if
       // if the orientation is vertical
        if (orientation == vertical)
        {
            // while the generated ramdom number plus the length of the ship is out of the grid
            while (cruiserX + cruiser >= row) { cruiserX = rand() % row;} // generate new y location
            for(int i = cruiserX; i < cruiserX + cruiser; i++)
            {
                  // check if the new location and the length of the ship is occupied with ship
                if(generateComputerships[i][cruiserY] == 'C' || generateComputerships[i][cruiserY] == 'B' || generateComputerships[i][cruiserY]== 'R' || generateComputerships[i][cruiserY] == 'S' || generateComputerships[i][cruiserY] == 'D')
                        // if there is a ship at the position start again
                    goto startHere;
            }
             // if there is no ship at this position set the cruiser ship
            for (int i = cruiserX; i < cruiserX + cruiser; i++)
            {
                generateComputerships[i][cruiserY] = 'R';
            }
            
        }// end if
        
        
    }
    
       // void function that generate random submarine ship
    void Computer::generateSubmarineShip()
    {
        // call function that generate cruiser ship
        generateCruiserShip();
        // to generate different random numbers
        srand(int (time(NULL)));
        // set length of submarine ship
        int submarine = 3;
        int vertical = 1; // set vertical = 1
        int horizontal = 0; // set horizontal = 0
        int orientation, submarineX, submarineY; // variable that holds x, y and orientation
        
    // lable
    startHere:
        do
        {
            
            orientation = rand() % 2; // generate vertical or horizontal
            submarineX = rand() % row; // generate random x location
            submarineY = rand() % col; // generate random y location
        } while (generateComputerships[submarineX][submarineY] == 'C' || generateComputerships[submarineX][submarineY] == 'B' || generateComputerships[submarineX][submarineY] == 'R' || generateComputerships[submarineX][submarineY]== 'S' || generateComputerships[submarineX][submarineY] == 'D'); // check if the location has a ship
        
          // if the orientation is vertical
        if (orientation == horizontal)
        {
             // while the generated ramdom number plus the length of the ship is out of the grid
            while (submarineY + submarine >= col) { submarineY = rand() % col;} // generate new y location
            for(int i = submarineY; i < submarineY + submarine; i++)
            {
                // check if the new location and the length of the ship is occupied with ship
                if(generateComputerships[submarineX][i] == 'C' || generateComputerships[submarineX][i]  == 'B' || generateComputerships[submarineX][i]  == 'R' || generateComputerships[submarineX][i] == 'S' || generateComputerships[submarineX][i]  == 'D')
                   // if there is a ship at the position start again
                    goto startHere;
            }
             // if there is no ship at this position set the submarine ship
            for (int i = submarineY; i < submarineY + submarine; i++)
            {
                generateComputerships[submarineX][i] = 'S';
            }
            
        } // end if
        
             // if the orientation is verticak
        if (orientation == vertical)
        {
             // while the generated ramdom number plus the length of the ship is out of the grid
            while (submarineX + submarine >= row) { submarineX = rand() % row;} // generate new x location
            for(int i = submarineX; i < submarineX + submarine; i++)
            {
                 // check if the new location and the length of the ship is occupied with ship
                if(generateComputerships[i][submarineY] == 'C' || generateComputerships[i][submarineY]  == 'B' || generateComputerships[i][submarineY]  == 'R' || generateComputerships[i][submarineY] == 'S' || generateComputerships[i][submarineY] == 'D')
                     // if there is a ship at the position start again
                    goto startHere;
            }
             // if there is no ship at this position set the submarine ship
            for (int i = submarineX; i < submarineX + submarine; i++)
            {
                generateComputerships[i][submarineY] = 'S';
            }
            
        }// end if
        
        
    }
       // void function that generate random detroyer ship
    void Computer::generateDetroyerShip()
    {
        // call fucntion that generate submarine ship
        generateSubmarineShip();
        // to generate different random number
        srand(int (time(NULL)));
        // set the length of detroyer ship
        int detroyer = 2;
        int vertical = 1; // set vertical = 1
        int horizontal = 0; // set horizontal = 1
        int orientation, detroyerX, detroyerY; // declare variables that holds x, y and orientations
        
     // lable
    startHere:
        do
        {
            
            orientation = rand() % 2; // generate verticla or norizontal
            detroyerX = rand() % row; // generate random x location
            detroyerY = rand() % col; // generate random y location
        } while (generateComputerships[detroyerX][detroyerY] == 'C' || generateComputerships[detroyerX][detroyerY]  == 'B' || generateComputerships[detroyerX][detroyerY]  == 'R' || generateComputerships[detroyerX][detroyerY] == 'S' || generateComputerships[detroyerX][detroyerY] == 'D'); // check if the location has a ship
        // if orientation is horizontal
        if (orientation == horizontal)
        {
               // while the generated ramdom number plus the length of the ship is out of the grid
            while (detroyerY + detroyer >= col) { detroyerY = rand() % col;} // generate new y location
            for(int i = detroyerY; i < detroyerY + detroyer; i++)
            {
                 // check if the new location and the length of the ship is occupied with ship
                if(generateComputerships[detroyerX][i] == 'C' || generateComputerships[detroyerX][i]  == 'B' || generateComputerships[detroyerX][i]  == 'R' || generateComputerships[detroyerX][i]== 'S' || generateComputerships[detroyerX][i] == 'D')
                   // if there is a ship at the position start again
                    goto startHere;
            }
              // if there is no ship at this position set the detroyer ship
            for (int i = detroyerY; i < detroyerY + detroyer; i++)
            {
                generateComputerships[detroyerX][i] = 'D';
            }
            
        }// end if
        // if orientation is veritical
        if (orientation == vertical)
        {    // while the generated ramdom number plus the length of the ship is out of the grid
            while (detroyerX + detroyer >= row) { detroyerX = rand() % row;}  // check if the location has a ship
            for(int i = detroyerX; i < detroyerX + detroyer; i++)
            {
                 // check if the new location and the length of the ship is occupied with ship
                if(generateComputerships[i][detroyerY] == 'C' || generateComputerships[i][detroyerY]  == 'B' || generateComputerships[i][detroyerY]  == 'R' || generateComputerships[i][detroyerY] == 'S' || generateComputerships[i][detroyerY] == 'D')
                     // if there is a ship at the position start again
                    goto startHere;
            }
             // if there is no ship at this position set the detroyer ship
            for (int i = detroyerX; i < detroyerX + detroyer; i++)
            {
                generateComputerships[i][detroyerY] = 'D';
            }
            
        }// end if
        
        
    }

// void function that prints the location ot the computer ships
    void Computer::print()
    {
        // display the location of computer ship using for loop
        cout << "\nlocation of the computer shipes " << endl;
        string  rowL[] = {"A", "B", "C", "D", "E" , "F", "G", "H", "I", "J"};
        cout << "     ";
        for (int i = 0; i < row; i++)
            cout  <<  rowL[i] << setw(3);
        cout << endl;
        for( int i = 0; i < row; i++ )
        {
            cout << i+1 << setw(3);
            for (int j = 0; j < col; j++)
            {
                // Display all the grid location of the ship
                cout << generateComputerships[i][j] << setw(3);
            }
            cout << endl;
            
        }
    
    }
 
    // bool function that returns true if the  if the computer hits all the player ship
    bool Computer::winerComputer()
    {
        // declare varible that count every hit
        int count = 0;
        bool flag = false;
        for( int i = 0; i < 10; i++ )
        {
            for (int j = 0; j < 10; j++)
            {
                // count all the hits on the grid
                if(computerGame[i][j]== 'O')
                    count++;

            }
        }
        
        // if the count is 17 change the status flag to true
        // the length of all ships is 5+4+3+3+2 = 17
        if(count == 17)
            flag = true;
        // return flag
        return flag;
    }
    
    
    
} // end BattleShip namespace

