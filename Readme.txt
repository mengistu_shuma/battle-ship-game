*******************************************************
*  Name      : Mengistu Shuma         
*  Student ID: 108458839                
*  Class     : CSCI 2421 001           
*  HW#       :  Final project               
*  Due Date  :  November 28, 2017
*******************************************************


                 Read Me

*******************************************************
*  Description of the program
*******************************************************
 	This program is a battleship game. There are two players for this program user
	and compute.The program reads data from file to set the location of the 
	ships for player and generate random location ship for the computer. The program 
	obtain the user torpedo fire from the user and generate random torpedo shot for 
	computer. The program checks whether the torpedo hits the opponent ship or 
	not. If the torpedo hit the ship, the program  marks as hit on it's grid.
	if the shot misses the ship, the program marks miss on it's grid. If all of the 
	ship hit by the opponent torpedo, the opponent wins the game.

	**** EXTRA CREDIT***

	I have done the extra credit.
	The program displays which ship is hit by the opponent torpedo and 
	also display "you sunk my ship!!" and the type of the ship when all of the length of 
	the ship hit by the opponent torpedo shot.

*******************************************************
*  Source files
*******************************************************

Name:  main.cpp
	calls function that reads data from file. After the program arranges both the
	computer and user ship location, the program prompts fire a torpedo to a user. If 
	the user shot hit the computer ship, the program display which ship the player
	hit and does the same for the computer. The program prompt to user if they
 	want to quit the game. If the user quit the game, the main call a function that
	displays the location of computer ship and the user guess. finally, the program
	displays the winer of the game.
	
Name: computer.h
	contain the definition of computer class

Name: computer.cpp
	contains the implementation of the computer class
Name: player.h
	contains the definition of the player class and inherited the public function 
	of computer class.
Name: player.cpp
	contain the implementation  of player class.
Name: exceptionHandling.h
	contain the definition and the implementation of exceptionHandling

   
*******************************************************
*  Circumstances of programs
*******************************************************

  The program runs successfully both on csegrid and Xcode.	
  The extra credit also runs successfully. 

*******************************************************
*  How to build and run the program
*******************************************************

1. The homework file is compressed. Uncompress the homework.  

   Now you should see a directory named FinalProject with the files:
        main.cpp
	computer.h
	computer.cpp
	player.h
	player.cpp
        makefile
        Readme.txt
	exceptionHandilg.h
	finalReport.pdf

2. Build the program.

    Change to the directory that contains the file by:
    cd FinalProject

    Compile the program by:
    make

3. Run the program by:
   ./final

4. Delete the obj files, executables, and core dump by
   make clean



