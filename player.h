// Mengistu Shuma ID 108458839
// CSCI 2312 001
// Assigment Final Project
// Battleship program
// November 28, 2017
#ifndef PLAYER_H
#define PLAYER_H


#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include "computer.h"
using namespace std;

// namespace used for this BattleShip game
namespace BattelShipGame {
    
    // class player and herited public class of computer class
    class Player: public Computer
    {
        // define and declare private variables of the player class
    private:
        static const int row = 10;
        static const int col = 10;
        
        // public function of player class
    public:
        
        // deafult constructor
        Player();
        void filltheGrid(); // fucntion that fills the grid by '-'
        void print(); // fucntion that print the grid
        void readFromFile(ifstream& infile); // fucntions that reads data from file
        char setPlayerShip[row][col]; // player grid
        void setCarrierShip(); // funtion that sets the palyer carrier ship from the file to grid
        void setBattleShip();  // funtion that sets the palyer battle ship from the file to grid
        void setCruiserShip();  // funtion that sets the palyer cruiser ship from the file to grid
        void setSubmarineShip();  // funtion that sets the palyer submarine ship from the file to grid
        void setDetroyerShip();  // funtion that sets the palyer detroyer ship from the file to grid
        bool winerPlayer(); // function that display if the palyer will
        void PlayerInput(char my[row][col]); // function that obtian the user guess
        void compare(char my[10][10]); // funtion that comapre if it is hit or miss
        char playerGame[row][col]; // player grid that records hit or miss
        string shipp[5], locationn[5], orientationn[5]; // array that hold type, location, and orientation of the ship
        
    };
}
#endif
