// Mengistu Shuma ID 108458839
// CSCI 2312 001
// Assigment Final Project
// Battleship program
// November 28, 2017

#ifndef EXCEPTIONHANDLING_H
#define EXCEPTIONHANDLING_H
#include <iostream>
#include <string>

using namespace std;

// using namespace BattleshipGame
namespace BattelShipGame {
    
    // class exceptionHandling
    class ExceptionHandling
    {
        // public
    public:
        // deafult constructor
        ExceptionHandling()
        
        { message = "Error reading the file. Please check the file"; } // show message if there is no data to read
        
        ExceptionHandling(string str)
        {   message = str;
        }
        
      string getMessage() const
        {
            return message;
        }
      // private variable
    private:
        string message;
      
    };
}
#endif
