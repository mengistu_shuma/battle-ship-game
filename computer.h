// Mengistu Shuma ID 108458839
// CSCI 2312 001
// Assigment Final Project
// Battleship program
// November 28, 2017

#ifndef COMPUTER_H
#define COMPUTER_H

#include <string>
#include <vector>
using namespace std;



// namespace sued for the gmae
namespace BattelShipGame {
    
 // Computer class
    class Computer
    {
        // private function of the computer class
    private:
        static const int row = 10; // declare row = 10;
        static const int col = 10; // declare col = 10;
      
        // public fucntion of the computer class
    public:
        Computer(); // defualt constructor
        ~Computer(); // distructor
        // function that generate computer torpedo shoot and loction of player ships
        void generateTorpedo(char play[10][10]);
        // void function that fill the computer grid with '-'
        void fillComputer();
        // virtual function that prints the location fo computer ships
        virtual void print();
        // void function that random generate the location of the carrier ship
        void generateCarrierShip();
         // void function that random generate the location of the Battlesship
        void generateBattleShip();
         // void function that random generate the location of the cruiser ship
        void generateCruiserShip();
         // void function that random generate the location of the submarine ship
        void generateSubmarineShip();
         // void function that random generate the location of the detroyer ship
        void generateDetroyerShip();
        // return true if the computer hits all locations of the player ship
        bool winerComputer();
        // computer grid that holds the location of the ship
        char generateComputerships[row][col];
        // computer grid that record hit or miss for fired torpedo
        char computerGame[row][col];
        
    };// end cumputer
}// end BattleShip
#endif
