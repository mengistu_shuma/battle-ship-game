// Mengistu Shuma ID 108458839
// CSCI 2312 001
// Assigment Final Project
// Battleship program
// November 28, 2017

#include <iostream>
#include "computer.h"
#include "player.h"
#include <fstream>
using namespace BattelShipGame; // include namespase used for the program
using namespace std;

// main function
int main()
{
    // declare object of the class
    Computer myComputer;
    Player myPlayer;
    string choice; // varibale that holds user input
    
    
    // Open file name called ship_placement.csv
    ifstream infile("ship_placement.csv");
    // call player object funtion that read from file
    myPlayer.readFromFile(infile);
    // fill the player frid with '-'
    myPlayer.filltheGrid();
    // call player function that set the location of all ships on the grid
    myPlayer.setDetroyerShip();
    // fill the computer grid with '-'
    myComputer.fillComputer();
    // call computer function that randomly generat the location of computer ships
    cout << "    Welcome to Battle ship Game!!" << endl;
    myComputer.generateDetroyerShip();

    // use do while loop to loop the game untile the winner known
    do {
   
        // call function that generate romdom torped shoot
        // and evaluate if it is hit or miss
         myComputer.generateTorpedo(myPlayer.setPlayerShip);
        cout << endl;
        // call function that ask and comper the user input
        // if it is hit or miss
        myPlayer.PlayerInput(myComputer.generateComputerships);
        
        // if the player hit all the ships of computer
        // the winnerPlayer function return true and the player win the game
        if(myPlayer.winerPlayer() == true)
        {
            cout <<"Game over" << endl;
            cout << "You won!!" << endl;
            return 0;
        }
        // if the computer hit all the ships of playert
        // the winnerComputer function return true and the computer win the game
        if(myComputer.winerComputer() == true)
        {   cout << "Game over" << endl;
            cout << "Computer won!!" << endl;
            return 0;
        }
        
        // Ask the user if they want to quit the game
        cout << "Enter 'Q' to quit: " ;
        cin >> choice;
        
        // If the user quit the game
        if(choice == "Q")
        {
          
            cout << "\n\tYou quit the game" << endl;
            //Display location of computer ships
            myComputer.print();
            // diplay the user guess
            myPlayer.print();
        }
     
        
    } while(choice != "Q"); // while the user input is not 'Q' loop the game
    
    cout << endl;
    return 0;
}// end main

